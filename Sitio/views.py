from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect

# Importando los modelos
from .models import Usuario, Noticia, Palabra, Frase

# Cosas para la autentificación
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.auth.decorators import login_required

# Cargar contenido dentro de los modal
from django.views.generic import UpdateView, ListView

# Templates
def index(request):
    usuario = request.session.get('usuario',None)
    contador  = Frase.objects.filter(estado='Pendiente').count() + Palabra.objects.filter(estado='Pendiente').count() + Noticia.objects.filter(estado='Pendiente').count()
    return render(request, 'Index.html', { 'usuario':usuario, 'noticias':Noticia.objects.all().filter(tag='General', estado='Público').order_by('-id')[:3],
        'carrousel':Noticia.objects.all().filter(tag='Carrousel', estado='Público').order_by('-id')[:3], 'destacado':Noticia.objects.all().filter(tag='Destacado', estado='Público').order_by('-id')[:1], 'contador':contador })

def frases(request):
    usuario = request.session.get('usuario',None)
    contador  = Frase.objects.filter(estado='Pendiente').count() + Palabra.objects.filter(estado='Pendiente').count() + Noticia.objects.filter(estado='Pendiente').count()
    return render(request, 'Frases.html', { 'usuario':usuario, 'frases':Frase.objects.all().order_by('titulo').filter(estado='Público'), 'contador':contador })

def diccionario(request):
    usuario = request.session.get('usuario',None)
    contador  = Frase.objects.filter(estado='Pendiente').count() + Palabra.objects.filter(estado='Pendiente').count() + Noticia.objects.filter(estado='Pendiente').count()
    return render(request, 'Diccionario.html', { 'usuario':usuario, 'diccionario':Palabra.objects.all().order_by('titulo').filter(estado='Público'), 'contador':contador })

@login_required(login_url='login')
def pendientes(request):
    usuario = request.session.get('usuario',None)
    contador  = Frase.objects.filter(estado='Pendiente').count() + Palabra.objects.filter(estado='Pendiente').count() + Noticia.objects.filter(estado='Pendiente').count()
    return render(request, 'Pendientes.html', { 'usuario':usuario, 'noticias':Noticia.objects.all().order_by('-id'), 'frases':Frase.objects.all().order_by('-id'), 'diccionario':Palabra.objects.all().order_by('-id'), 'contador':contador })

def registro(request):
    usuario = request.session.get('usuario',None)
    contador  = Frase.objects.filter(estado='Pendiente').count() + Palabra.objects.filter(estado='Pendiente').count() + Noticia.objects.filter(estado='Pendiente').count()
    return render(request,'Registro.html', { 'usuario':usuario, 'contador':contador })

# Crear un nuevo usuario desde el Registro
def crear(request):
    email = request.POST.get('email',False)
    cpassword = request.POST.get('cpassword',False)
    usuario = Usuario(email=email, password=cpassword)
    usuario.set_password(cpassword)
    usuario.save()
    return redirect("index")

####################
#    NOTICIAS
####################
# Agregar una noticia
@login_required(login_url='login')
def nuevaNoticia(request):
    titulo = request.POST.get('titulo',False)
    descripcion = request.POST.get('descripcion',False)
    tag = request.POST.get('tag',False)
    foto = request.FILES['foto']
    noticia = Noticia(titulo=titulo, descripcion=descripcion, estado='Pendiente', tag=tag, foto=foto)
    noticia.save()
    return redirect('index')

# Editar una noticia
@login_required(login_url='login')
def editarNoticia(request):
    titulo = request.POST.get('editTituloN',False)
    descripcion = request.POST.get('editDescripcionN',False)
    estado = request.POST.get('editEstadoN',False)
    tag = request.POST.get('editTagN',False)
    foto = request.FILES['editFoto']
    id = request.POST.get('editIdN',0)
    noticia = Noticia.objects.get(pk = id)
    noticia.titulo = titulo
    noticia.descripcion = descripcion
    noticia.estado = estado
    noticia.tag = tag
    noticia.foto = foto
    noticia.save()
    return redirect('index')

# Publicar una noticia
@login_required(login_url='login')
def publicarNoticia(request,id):
    noticia = Noticia.objects.get(pk = id)
    noticia.estado = 'Público'
    noticia.save()
    return redirect('index')

# Borrar una noticia
@login_required(login_url='login')
def borrarNoticia(request,id):
    notitica = Noticia.objects.get(pk = id)
    notitica.delete()
    return redirect('index')

####################
#    DICCIONARIO
####################
# Agregar una palabra
@login_required(login_url='login')
def nuevaPalabra(request):
    titulo = request.POST.get('titulo',False)
    descripcion = request.POST.get('descripcion',False)
    palabra = Palabra(titulo=titulo, descripcion=descripcion, estado='Pendiente')
    palabra.save()
    return redirect('diccionario')

# Editar una palabra
@login_required(login_url='login')
def editarPalabra(request):
    titulo = request.POST.get('editTitulo',False)
    descripcion = request.POST.get('editDescripcion',False)
    estado = request.POST.get('editEstado',False)
    id = request.POST.get('editId',0)
    palabra = Palabra.objects.get(pk = id)
    palabra.titulo = titulo
    palabra.descripcion = descripcion
    palabra.estado = estado
    palabra.save()
    return redirect('diccionario')

# Publicar una palabra
@login_required(login_url='login')
def publicarPalabra(request,id):
    palabra = Palabra.objects.get(pk = id)
    palabra.estado = 'Público'
    palabra.save()
    return redirect('diccionario')

# Borrar una palabra
@login_required(login_url='login')
def borrarPalabra(request,id):
    palabra = Palabra.objects.get(pk = id)
    palabra.delete()
    return redirect('diccionario')

####################
#    FRASES
####################
# Agregar una frase
@login_required(login_url='login')
def nuevaFrase(request):
    titulo = request.POST.get('titulo',False)
    descripcion = request.POST.get('descripcion',False)
    frase = Frase(titulo=titulo, descripcion=descripcion, estado='Pendiente')
    frase.save()
    return redirect('frases')

# Editar una frase
@login_required(login_url='login')
def editarFrase(request):
    titulo = request.POST.get('editTituloF',False)
    descripcion = request.POST.get('editDescripcionF',False)
    estado = request.POST.get('editEstadoF',False)
    id = request.POST.get('editIdF',0)
    frase = Frase.objects.get(pk = id)
    frase.titulo = titulo
    frase.descripcion = descripcion
    frase.estado = estado
    frase.save()
    return redirect('frases')

# Publicar una frase
@login_required(login_url='login')
def publicarFrase(request,id):
    frase = Frase.objects.get(pk = id)
    frase.estado = 'Público'
    frase.save()
    return redirect('frases')

# Borrar una frase
@login_required(login_url='login')
def borrarFrase(request,id):
    frase = Frase.objects.get(pk = id)
    frase.delete()
    return redirect('frases')

####################
#    LOGIN
####################
# Autentificación template
def login(request):
    return render(request,'Login.html',{})

# La acción de iniciar sesión
def loginIniciar(request):
    email = request.POST.get('email',False)
    contrasenia = request.POST.get('contrasenia',False)
    usuario = authenticate(request,username=email, password=contrasenia)
    if usuario is not None:
        request.session['usuario'] = usuario.email
        auth_login(request, usuario)
        return redirect('index')
    else:
        return redirect("login")

# Cerrando sesión
@login_required(login_url='login')
def cerrarSession(request):
    del request.session['usuario']
    logout(request)
    return redirect('index')