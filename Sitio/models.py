from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)

# Crear usuarios
class PersonalizadoBaseUserManager(BaseUserManager):
    def create_user(self, email,password):
        user = self.model(email=email)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self,email,password):
        user = self.create_user(email,password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user
       
# Modelos
class Noticia(models.Model):
    titulo = models.CharField(max_length=40)
    descripcion = models.CharField(max_length=500)
    estado = models.CharField(max_length=40)
    tag = models.CharField(max_length=40)
    foto = models.ImageField(upload_to='fotos/')

class Palabra(models.Model):
    titulo = models.CharField(max_length=40)
    descripcion = models.CharField(max_length=500)
    estado = models.CharField(max_length=40)

class Frase(models.Model):
    titulo = models.CharField(max_length=40)
    descripcion = models.CharField(max_length=500)
    estado = models.CharField(max_length=40)

class Usuario(AbstractBaseUser, PermissionsMixin):
    email = models.CharField(max_length=100, unique=True)
    password = models.CharField(max_length=100)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    USERNAME_FIELD = 'email'
    objects = PersonalizadoBaseUserManager()
    def get_full_name(self):
        return self.email